Source: gnome-mplayer
Section: video
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Aron Xu <aron@debian.org>,
 Ross Gammon <rossgammon@mail.dk>
Build-Depends:
 debhelper-compat (= 9),
 dh-autoreconf,
 libpulse-dev,
 libx11-dev,
 libxss-dev,
 libnautilus-extension-dev,
 libgtk-3-dev,
 libgmtk-dev (>= 1.0.7),
 libdbus-glib-1-dev,
 libnotify-dev,
 libmusicbrainz3-dev (>= 3.0.2-2.1),
 libcurl4-gnutls-dev,
 libgpod-dev [!hurd-i386],
 libimobiledevice-dev [!hurd-i386],
 pkg-config,
 libgda-5.0-dev,
 libgconf2-dev
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/multimedia-team/gnome-mplayer
Vcs-Git: https://salsa.debian.org/multimedia-team/gnome-mplayer.git
Homepage: http://code.google.com/p/gnome-mplayer

Package: gnome-mplayer
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 mplayer2 | mplayer
Recommends:
 gnome-icon-theme-symbolic,
 xdg-utils
Suggests:
 gecko-mediaplayer
Description: GTK+ interface for MPlayer
 The power of MPlayer combined with a friendly interface for your desktop.
 You can play all your multimedia (audio, video, CD, DVDs, and VCDs, streams
 etc. with full DVD and MKV chapter support), organize, sort and create
 playlists, take screenshots while playing videos, be notified about media
 changes, retrieve cover art and more.
 Gnome-MPlayer fully supports subtitles giving the ability to specify
 preferred audio and subtitle languages.
 .
 The player can be used to play media on websites from your browser when
 used with Gecko Mediaplayer and is the modern replacement for
 mplayerplug-in (from the same author).

Package: gnome-mplayer-dbg
Section: debug
Architecture: any
Depends:
 ${misc:Depends},
 gnome-mplayer (= ${binary:Version})
Description: GTK+ interface for MPlayer (debugging symbols)
 The power of MPlayer combined with a friendly interface for your desktop.
 You can play all your multimedia (audio, video, CD, DVDs, and VCDs, streams
 etc. with full DVD and MKV chapter support), organize, sort and create
 playlists, take screenshots while playing videos, be notified about media
 changes, retrieve cover art and more.
 Gnome-MPlayer fully supports subtitles giving the ability to specify
 preferred audio and subtitle languages.
 .
 The player can be used to play media on websites from your browser when
 used with Gecko Mediaplayer and is the modern replacement for
 mplayerplug-in (from the same author).
 .
 This package contains the debugging symbols.
